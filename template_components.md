Komponenty pro HTML
-------------------

Angular poskytuje mnoho komponentů pro práci s DOM. Začínají prefixem `ng-` například `ng-app`, `ng-controller`.

S dvěma komponenty jsme se už setkali. S `ng-app` která obaluje celý Angular projekt a `ng-controller` který určuje controller pro danou část projektu.

Všechny následující ukázky jsou spravovány tímto controllerem.

    <script type="text/javascript">
    function MainController($scope){
        $scope.greeting = "Hello world!";

        $scope.list = ["Hi,", "how", "are", "you?"];
        
        $scope.alert = function(){alert("Hello boys");}
    }
    </script>

### Zobrazení obsahu

Nejjedoduší zobrazení obsahu je

    <p>{{greeting}}</p>

nebo můžeme použít komponentu `ng-bind` který vkládá obsah proměnné jako své `innerHtml`.

    <p ng-bing="greeting"></p>

Oba způsoby jsou shodné.


### Opakování

Opakovaní daného objektu zajištuje komponenta `ng-repeat`

    <p><span ng-repeat="word in list">{{word}} </span></p>
    
Prohlížeč vypíše

     Hi, how are you?
     

### Zobrazení / skrývání

Pokud je hodnota proněnné komponenty `ng-shoe` true zobrazí daný blok. Pokud ne, skryje ho.

`ng-hide` je inverzní fuknkcí k `ng-show`


### Načítání dat z formuláře

Komponenta `ng-model` obsahuje hodnotu `value` daného inputu.

    <input type="text" ng-model="text">
    <p>{{text}}</p>

Tento kód zrcadlí obsah textového pole do řádky pod ním.

### Akce

Po kliknutí na blok spustí funkci která je poskytována objektem `$scope`.

    <a href="#" ng-click="alert()">Alert</a>
    
Po kliknutí na tento odkaz vyskočí alert okno prohlížeče.