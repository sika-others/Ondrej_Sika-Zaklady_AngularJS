První Angular aplikace
----------------------

Nejdříve si vytvoříme si složku pro naše projekty

    sika@ntb:~$ mkdir angular
    sika@ntb:~$ cd angular
    sika@ntb:~/angular$ 

a stáhneme angular.

    sika@ntb:~/angular$ wget .../angular.min.js

Ve složce `~/angular` si spustíme HTTP server příkazem

    sika@ntb:~/angular$ python -m SimpleHTTPServer &

Vytvoříme si složku pro náš první projekt.

    sika@ntb:~/angular$ mkdir 01
    sika@ntb:~/angular$ cd 01
    sika@ntb:~/angular/01$

Zde můžeme vytvořit naši první angular aplikaci. Vytvořme zde soubor `index.html` s obsahem

    <html ng-app>
        <head>
            <title>My first Angular app</title>
            <script type="text/javascript" src="../angular-1.0.8.min.js"></script>
            <script type="text/javascript">
            function MainController($scope){
                $scope.greeting = "Hello world!";
            }
            </script>
        </head>
        <body ng-controller="MainContoller">
            {{greeting}}
        </body>
    </html>

Na stránku se můžeme podívat v prohlížeči na adrese `localhost:8000/01/index.html`

Objekt `$scope` poskytuje proměnné controllru v templatu.